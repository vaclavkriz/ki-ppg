﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PPG
{

    class Graph
    {
        private int n;
        double[,] matrix;
        List<Position> p;
        Stopwatch s = new Stopwatch();

        public Graph(int n)
        {
            this.n = n;
            matrix = new double[n, n];
            p = new List<Position>();
            Random r = new Random();
            for (int i = 0; i < n; i++)
            {
                p.Add(new Position(r.NextDouble(), r.NextDouble()));
            }
        }

        public void insertRandomEdge()
        {
            Random r = new Random();
            while (true)
            {
                int fromVertex = r.Next(n);
                int toVertex = r.Next(n);
                if (fromVertex == toVertex)
                    continue;
                if (matrix[fromVertex, toVertex] > 0.0)
                    continue;
                double dist = p[fromVertex].QDistance(p[toVertex]);
                double prob = (2.0 - dist) * (2.0 - dist) / 4.0;
                lock (matrix)
                {
                    if (r.NextDouble() < prob)
                    {
                        matrix[fromVertex, toVertex] = matrix[toVertex, fromVertex] = Math.Sqrt(dist);
                        break;
                    }
                }
            }
        }

        public void insertRandomEdges(int n)
        {
            s.Start();

            for (int i = 0; i < n; i++)
                insertRandomEdge();

            s.Stop();
            Console.WriteLine(s.Elapsed + " ; Serial");
            s.Reset();
        }
        //done -- správně umístit lock a je to rychlejší než serial
        public void insertRandomEdgesParal(int n)
        {
            s.Start();

            Parallel.For(0, n, i =>
            {
                insertRandomEdge();
            });

            s.Stop();
            Console.WriteLine(s.Elapsed + " -> Parallel");
            s.Reset();
        }
        //tento serial je moc fast
        public bool IsGraphConnected()
        {
            s.Start();

            var valid = 0;

            for (int i = 0; i < this.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < this.matrix.GetLength(0); j++)
                {
                    if (this.matrix[i, j] != 0.0)
                    {
                        valid++;
                        break;
                    }
                    else { continue; }
                }
            }

            s.Stop();
            Console.WriteLine(s.Elapsed + " -> Serial");
            s.Reset();

            return valid == this.matrix.GetLength(0);
        }
        //už jede, ale pomalejší než serial
        public bool IsGraphConnectedParal()
        {
            s.Start();

            var numbers = new List<int>();

            Parallel.For(0, this.matrix.GetLength(0), i =>
            {
                int isOk = 0;
                Parallel.For(0, this.matrix.GetLength(0), j =>
                {

                    if (this.matrix[i, j] != 0.0)
                    {
                        Interlocked.Exchange(ref isOk, 1);
                    }
                });
                lock (numbers)
                {
                    if (isOk == 1)
                    {
                        numbers.Add(1);
                    }
                    else
                    {
                        numbers.Add(0);
                    }
                }
                isOk = 0;
            });

            s.Stop();
            Console.WriteLine(s.Elapsed + " -> Parallel");
            s.Reset();

            return (numbers.IndexOf(0) == -1);
        }
        //done, ale je to Shortest Path First, takže nic moc
        public void SPF(int start, int end)
        {
            s.Start();

            double[] availableVertices = new double[this.n];
            for (int i = 0; i < this.n; i++)
            {
                availableVertices[i] = 2;
            }
            availableVertices[start] = 0;
            double path = 0;
            double lowest;
            int newStart = 0;
            while (start != end)
            {
                lowest = 2;
                for (int i = 0; i < this.n; i++)
                {
                    if (this.matrix[start, i] != 0 && this.matrix[start, i] < availableVertices[i])
                    {
                        availableVertices[i] = this.matrix[start, i];
                    }
                    if (availableVertices[i] != 0 && availableVertices[i] < lowest)
                    {
                        lowest = availableVertices[i];
                        newStart = i;
                        if (i == end)
                            break;
                        else
                            continue;
                    }
                }
                start = newStart;
                availableVertices[start] = 0;
                path += lowest;
                //Console.WriteLine(string.Format("{0:0.0}  ", lowest));
            }
            Console.WriteLine(string.Format("{0:0.0}  ", path));

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //done
        public void SPFParal(int start, int end)
        {
            s.Start();

            double[] availableVertices = new double[this.n];
            Parallel.For(0, this.n, i =>
            {
                availableVertices[i] = 2;
            });
            availableVertices[start] = 0;
            double path = 0;
            double lowest;
            int newStart = 0;
            while (start != end)
            {
                lowest = 2;
                Parallel.For(0, this.n, (i, state) =>
                {
                    if (this.matrix[start, i] != 0 && this.matrix[start, i] < availableVertices[i])
                    {
                        availableVertices[i] = this.matrix[start, i];
                    }
                    if (availableVertices[i] != 0 && availableVertices[i] < lowest)
                    {
                        lowest = availableVertices[i];
                        newStart = i;
                        if (i == end)
                            state.Break();
                    }
                });
                start = newStart;
                availableVertices[start] = 0;
                path += lowest;
                //Console.WriteLine(string.Format("{0:0.0}  ", lowest));
            }
            Console.WriteLine(string.Format("{0:0.0}  ", path));

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //done
        public void MinimalSkeleton()
        {
            s.Start();

            int start = 0;
            int[] vertices = new int[this.n];
            double[] availableVertices = new double[this.n];
            for (int i = 0; i < this.n; i++)
            {
                availableVertices[i] = 2;
            }
            availableVertices[start] = 0;
            vertices[start] = 0;
            double path = 0;
            double min;
            int newStart = 0;
            for (int k = 0; k < this.n - 1; k++)
            {
                min = 2;
                for (int i = 0; i < this.n; i++)
                {
                    if (this.matrix[start, i] != 0 && this.matrix[start, i] < availableVertices[i])
                    {
                        availableVertices[i] = this.matrix[start, i];
                    }
                    if (availableVertices[i] != 0 && availableVertices[i] < min)
                    {
                        min = availableVertices[i];
                        newStart = i;
                    }
                }
                start = newStart;
                vertices[k + 1] = start;
                availableVertices[start] = 0;
                path += min;
            }
            Console.WriteLine(string.Format("{0:0.0}  ", path));
            /*foreach (var item in vertices)
            {
                Console.Write(item + " -> ");
            }*/

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //tady je problém, že to bez výpisu vrací špatně, což nechápu
        public void MinimalSkeletonParallel()
        {
            s.Start();


            int[] vertices = new int[this.n];
            double[] availableVertices = new double[this.n];


            Parallel.For(0, this.n, i =>
            {
                lock (availableVertices)
                {
                    availableVertices[i] = 2;
                }
            });

            int start = 0;
            double path = 0;
            Parallel.For<double>(0, this.n - 1, () => 0, (k, loop, subtotal) =>
            {
                //Console.WriteLine(k);
                lock (availableVertices)
                {
                    availableVertices[start] = 0;
                }
                vertices[start] = 0;
                int newStart = 0;
                double min = 2;

                for (int i = 0; i < this.n; i++)
                {

                    if (this.matrix[start, i] != 0 && this.matrix[start, i] < availableVertices[i])
                    {
                        availableVertices[i] = this.matrix[start, i];
                    }
                    if (availableVertices[i] != 0 && availableVertices[i] < min)
                    {
                        min = availableVertices[i];
                        newStart = i;
                    }
                }

                Interlocked.Exchange(ref start, newStart);
                vertices[k + 1] = start;
                availableVertices[start] = 0;
                subtotal += min;
                return subtotal;
            },
             (x) => { /*Console.WriteLine(x);*/ Interlocked.Exchange(ref path, path + x); }
             );


            Console.WriteLine(string.Format("{0:0.0}  ", path)); //TODO: vyladit
                                                                 /*foreach (var item in vertices)
                                                                 {
                                                                     Console.Write(item + " -> ");
                                                                 }*/

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //tryout - lepší než to nad tím
        public void MinimalSkeletonParal()
        {
            s.Start();

            int start = 0;
            int[] vertices = new int[this.n];
            double[] availableVertices = new double[this.n];
            Parallel.For(0, this.n, i =>
            {
                availableVertices[i] = 2;
            });
            availableVertices[start] = 0;
            vertices[start] = 0;
            double path = 0;
            double min;
            int newStart = 0;
            for (int k = 0; k < this.n - 1; k++)
            {
                min = 2;
                Parallel.For(0, this.n, i =>
                {
                    if (this.matrix[start, i] != 0 && this.matrix[start, i] < availableVertices[i])
                    {
                        availableVertices[i] = this.matrix[start, i];
                    }
                    if (availableVertices[i] != 0 && availableVertices[i] < min)
                    {
                        min = availableVertices[i];
                        newStart = i;
                    }
                });
                Interlocked.Exchange(ref start, newStart);
                vertices[k + 1] = start;
                availableVertices[start] = 0;
                path += min;
            }
            Console.WriteLine(string.Format("{0:0.0}  ", path));

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //done
        public void Dijkstra(int src, int end)
        {
            s.Start();

            double temp = double.PositiveInfinity;
            int tempj = 0;
            List<int> unvisited = new List<int>();
            double[] distanceFromSrc = new double[this.n];
            for (int i = 0; i < this.n; i++)
            {
                unvisited.Add(i);
                distanceFromSrc[i] = double.PositiveInfinity;
            }
            distanceFromSrc[src] = 0;
            unvisited.Remove(src);
            for (int i = 0; i < this.n; i++)
            {
                temp = double.PositiveInfinity;
                for (int j = 0; j < this.n; j++)
                {
                    if (this.matrix[src, j] != 0 && (this.matrix[src, j] + distanceFromSrc[src]) < distanceFromSrc[j])
                    {
                        distanceFromSrc[j] = this.matrix[src, j] + distanceFromSrc[src];
                    }
                    if (distanceFromSrc[j] < temp && unvisited.Contains(j))
                    {
                        temp = distanceFromSrc[j];
                        tempj = j;
                    }
                }
                src = tempj;
                unvisited.Remove(src);
            }

            Console.WriteLine(string.Format("{0:0.0}  ", distanceFromSrc[end]));

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //done
        public void DijkstraParal(int src, int end)
        {
            s.Start();

            double temp = double.PositiveInfinity;
            int tempj = 0;
            List<int> unvisited = new List<int>();
            double[] distanceFromSrc = new double[this.n];
            for (int i = 0; i < this.n; i++)
            {
                unvisited.Add(i);
                distanceFromSrc[i] = double.PositiveInfinity;
            }
            distanceFromSrc[src] = 0;
            unvisited.Remove(src);
            for (int i = 0; i < this.n; i++)
            {
                temp = double.PositiveInfinity;
                Parallel.For(0, this.n, j =>
                {
                    if (this.matrix[src, j] != 0 && (this.matrix[src, j] + distanceFromSrc[src]) < distanceFromSrc[j])
                    {
                        distanceFromSrc[j] = this.matrix[src, j] + distanceFromSrc[src];
                    }
                    if (distanceFromSrc[j] < temp && unvisited.Contains(j))
                    {
                        temp = distanceFromSrc[j];
                        tempj = j;
                    }
                });
                src = tempj;
                unvisited.Remove(src);
            }

            Console.WriteLine(string.Format("{0:0.0}  ", distanceFromSrc[end]));

            s.Stop();
            Console.WriteLine(s.Elapsed);
            s.Reset();
        }
        //done
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.Append("    ");
            for (int i = 0; i < n; i++)
            {

                if (i.ToString().Length == 1)
                {
                    s.Append($"{i}    ");
                }
                else if (i.ToString().Length == 2)
                {
                    s.Append($"{i}   ");
                }
            }
            s.Append("\n");
            for (int i = 0; i < n; i++)
            {
                if (i.ToString().Length == 1)
                {
                    s.Append($"{i}  ");
                }
                else if (i.ToString().Length == 2)
                {
                    s.Append($"{i} ");
                }


                for (int j = 0; j < n; j++)
                {
                    s.Append(string.Format("{0:0.0}  ", matrix[i, j]));
                }
                s.Append("\n");
            }
            return s.ToString();
        }
    }

}
