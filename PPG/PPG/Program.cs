﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace PPG
{
   
    class MainClass
    {
        public static void Main(string[] args)
        {
            Graph g = new Graph(10000);
            g.insertRandomEdgesParal(450000);
            //g.insertRandomEdges(450000);

            //Console.WriteLine(g);

            if (g.IsGraphConnected())
            {
                g.SPF(0, 6);
                g.SPFParal(0, 6);
                //g.MinimalSkeletonParal();
                //g.MinimalSkeleton();
                //g.Dijkstra(0,6);
                g.DijkstraParal(0, 6);
            }
            else
            {
                Console.WriteLine("Graf není souvislý.");
            }
        }
    }
}
