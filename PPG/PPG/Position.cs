﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace PPG
{
    class Position
    {
        public double x;
        public double y;

        public Position(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double QDistance(Position that)
        {
            return (this.x - that.x) * (this.x - that.x) + (this.y - that.y) * (this.y - that.y);
        }
    }
}
